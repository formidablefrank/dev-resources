# Frequently Asked Questions

Here are some questions about software development we feel might be asked or have been asked frequently. Feel free to ask us more questions at email@email.com or Facebook. **Do note** that this guide will be more focused on web development.
***
## For newcomers to development

### What's a ___ ?

Here are some terms, buzzwords, or other things about software development that you might not know the meaning of:

* **Web Development**: Making software for the web (eg. websites, web apps, etc.)
* **Web Apps**: Think of apps in the conventional sense, but accessed with the web browser, as opposed to having to download and install it.
* **Frontend / Backend**: Frontend is what the user sees and can see; vice-versa for backend.
* **Client / Server**: Client refers to the user's computer. Server refers to a computer / program somewhere else that handles background computations. Generally speaking, frontend is processed by the client; backend is processed by the server.
* **Framework**: A set of libraries and program structures set to help you write code. See the section below for more detail.
* **API**: Application Programming Interface. It's a way for you to make programs while easily using the features of another program. Imagine getting Facebook comments from a post in only a few lines in your code, by calling a few functions. That's thanks to an API.
* **IDE**: Integrated Development Environment. It's a program where you can write, check, debug, and run code in one place, among other things. Examples include Eclipse for Java.

These are terms that are a little more technical:

* **Database**: A structure in a server where data is stored. You interact with it using a language like SQL (or structured query language).
* **ORM**: Object-Relational Mapping. Allows you to interact with the Database in a more Object-Oriented manner (as opposed to using something like SQL).
* **Design pattern**: A way of structuring your code and codebase to make certain actions and behaviors easier and less prone to error and bugs. This is a whole different topic altogether.
* **MVC**: A Design Pattern, usually for web apps. It stands for Model-View-Controller. You can find more details on it online. Rails uses this design pattern.
* **Opinionated**: Refers to things that guide or force you along a certain way of doing things. Usually it isn't the only way to do it, but the thing in question urges you to follow it. Usually used in the context of frameworks that urge you to follow their structure.
* **DOM**: Document Object Model. This refers to the structure of a web page, similar to that of a tree.
* **SPA**: Single-page application. Self-explanatory.
* **CRUD**: Stands for Create, Read, Update, Delete. This refers to a type of app where the basic functionality is the creation, reading, updating, and deleting of data.
* **REST / RESTful**: Stands for Representational State Transfer. This refers to a way systems (eg. web apps) in the Internet work together and communicate. RESTful describes something that uses REST. There's a lot more to this [here.](http://www.restapitutorial.com/lessons/whatisrest.html)

### What's the difference between a framework, a programming language, and a library?

A programming **language** is the language you write code on to make programs. A **library** is a set of code that you can borrow to help create programs. A **framework** is a set of libraries and structures which help you write code to make programs.

Very loosely speaking, if your **program** was a cake and your code the ingredients, the **language** you use are your hands, the **libraries** are the store-bought cake mix, frosting, and other pre-made parts, and the **framework** is the kitchen and set-up of tools.

### How do I start learning? There are so many different choices to start!

When starting in web development (or in any kind of development really), we suggest setting your environment and other tools first. Check out our [recommended development tools guide](https://gitlab.com/up-csi/dev-resources/blob/master/development_tools.md) first to start off.

Start off by learning the basics of client-side web development: **HTML** for defining the content and structure of your website, **CSS** for defining the style and look of your website, and **JavaScript** for defining how your website will function and react to the user.

These 3 languages have a lot of depth to them (a high "skill cap"). Don't worry about having to be a master in each of these 3 languages before going to the next step. A cursory understanding of each will be enough.

For server-side development (like stored data, and deciding which URL goes to which web page), you'll want to learn a backend programming language / framework. Usually people go with PHP and MySQL to start learning server-side web development, but with a lack of structure, this can give way to bad design habits and poorly-written code.

For this reason, we suggest starting off with **Ruby on Rails** as your first backend programming language and framework. Although the use of "magic words" make for a steeper learning curve than usual, the framework teaches you good programming practices and structures, and the syntax is similar to Python, allowing you to easily pick up on Ruby.

Do note that this is **just our suggestion.** For some people, they prefer to start with PHP. Others like to go with JavaScript or Python first. By all means do so, as long as you are comfortable with it.

From here on out, you can branch out and learn more stuff, depending on where you want to go or specialize in next. For guides and tutorials, check out our ReadMe page.

### Why do you use Rails a lot?

Because it's easy to learn, even for those new to web development. It also has certain rules set to prevent you from making bad design decisions (eg. bad, spaghetti, hard to understand code). The structures in place also make it easy to build simple web apps fast.

### I've learned the basics, now what?

Feel free to explore the field of web development more. Like frontend development? Or maybe backend? Maybe you want to do data science or AI? We have resources that cover some of what you might need for what you want to do, but feel free to search for more resources online.
***
## For those who have just started web development

### What is _insert framework / language / tool here_ ?

Here are a list of well-known frameworks and tools. You can use this as a reference on what to learn next.

For frontend development, other than HTML, CSS, and JS, there are also frameworks and tools available, with uses ranging from basic styling to structured handling of the DOM.

* **Bootstrap**: A frontend framework, used to include basic but visually appealing styles. Rather overused; it can be very obvious that you used Bootstrap.
* **SemanticUI**: Similar to Bootstrap
* **jQuery**: A JS library, meant to make JS client-side development easier and shorter.
* **AngularJS**: A JS framework used to help make more dynamic websites and web apps. Useful for SPAs. Developed by Google.
* **ReactJS**: A JS library that accomplishes largely the same things as AngularJS, but with a different method. Developed by Facebook.

For backend development, languages like PHP, Python, Ruby, and even JS are used. This is a short list of frameworks and tools for each language that you may find useful.

#### PHP
* **Laravel**: framework to make backend dev in PHP cleaner
* **Wordpress**: CMS for PHP; widely used for blog-like websites

#### Python
* **Django**: framework for Python backend dev; brings a lot of tools out of the box
* **Flask**: framework for Python backend dev; much more minimalist
* **SciPy / NumPy**: set of libraries and tools for scientific computing in Python

#### Ruby
* **Rails**: framework for Ruby backend dev; fast to use
* **Devise**: library in Ruby used for authentication

#### JS
* **NodeJS**: tool that allow JS to be ran in servers instead of just web browsers
* **ExpressJS**: framework for JS backend dev
* **SailsJS**: framework for JS backend dev; similar to Rails in design

### What language / framework / library should I use?

While it depends on the job at hand, you're usually better off using **whatever you're comfortable with.** Current libraries, tools, frameworks, etc. exist so you can write mobile apps with even JavaScript.

That being said, if you want to decide what language is best for the job, there are many answers online. Generally speaking though, you'd be using these in web development for the respective jobs:

* **PHP**: A well-known language in server-side dev.; good when dealing with existing systems in PHP in the industry, and stuff like Wordpress.
* **Python**: A general-purpose language; good when working with data science and math-heavy apps (using packages like SciPy)
* **JS**: A popular language with a huge community; good for web development (mostly client-side, but can also be done server-side), though libraries exist for making native and mobile apps.
* **Ruby**: Very similar to Python; usually used with Rails for easy-to-learn and fast development.
***
## For miscellaneous but relevant matters

### How do I get super-skilled / become a CS Lord?

These are just tips in general to help you in studying web development.
* Practice and program a lot. Cliche, but true.
* Think of something you want to make. It's easier to practice if you have a concrete goal in mind.
 * You can also think of something useful to make, like a program to help you with computing the optimal way to pre-enlist in CRS.
* Think of something that inspires you to code. Good for the initial slump, but not always for developing discipline.
* Don't be afraid to ask questions. No one knows everything; that's why we ask questions. If you're feeling lost in learning, even something like insight might help.
* Take breaks. Learning to dev can be really frustrating. Resting can also give you a fresh perspective, or new insights.
* Manage your time. Aside from studying dev, you have sleep, school lives, and social lives, and it would be best if you didn't sacrifice these.
 * At the same time, I personally suggest not putting too much time into academics and forsaking self-learning. Chances are, you'll be learning more by yourself in this topic than in the classroom.

These tips are more geared specifically towards writing code.
* Using a framework is helpful, but be wary of the crutches you might be getting yourself used to.
* Keep your code structured and clean. This makes your code less prone to bugs, and if ever your code gets bugs, they will be easier to deal with.
* Documentation is usually important, especially if you don't plan on finishing your app at once, working with a team, or if you plan to look at it again in the future.
