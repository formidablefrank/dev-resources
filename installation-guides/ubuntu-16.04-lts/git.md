## Instructions:
Just enter these two lines
```bash
sudo apt-get update
sudo apt-get install git
```

You might want to configure your credentials too
```bash
git config --global user.name "Your Name"
git config --global user.email "Your Email"
```

This will help when working with people of other operating systems (formatting wise)
```bash
git config --global core.autocrlf false
```