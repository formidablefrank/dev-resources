Here's some packages to get. I'll just list some and categorize them.

## For Ruby on Rails:
```bash
sudo apt-get update
sudo apt-get install sqlite3 libsqlite3-dev
sudo apt-get install autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libpq-dev libgdbm3 libgdbm-dev
```


If you find any dependancies for each tool please let me know. I just jumble all of these up in my laptop so I don't know exactly what's needed for future tools since I probalby already installed it