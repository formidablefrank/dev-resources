## Prerequisites:
 - [git](https://gitlab.com/up-csi/dev-resources/blob/master/Installation%20Guides/Ubuntu%2016.04%20LTS%20(Xenial%20Xerus)/git.md)

## Official Instructions:
[https://flight-manual.atom.io/getting-started/sections/installing-atom/](https://flight-manual.atom.io/getting-started/sections/installing-atom/)

## Instructions:

Download the .deb [here](https://atom.io/)

Move the terminal to where the directory of the package
```bash
sudo dpkg -i atom-amd64.deb
sudo apt-get -f install
```
From here you can use
```bash
atom
```
to launch or you can just pin it to taskbar
