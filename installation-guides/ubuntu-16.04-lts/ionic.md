## Prerequisites:
npm

## Official Instructions:
[https://ionicframework.com/getting-started/](https://ionicframework.com/getting-started/)

## Instructions:
```bash
npm install -g cordova ionic
```