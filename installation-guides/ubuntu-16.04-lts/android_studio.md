# WIP

## Official Instructions:
https://developer.android.com/studio/install.html

## Instructions:
You will need these libraries if using a 64 bit Ubuntu version (like me)
```bash
sudo apt-get install libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 libbz2-1.0:i386
```
Install the zip file [here](https://developer.android.com/studio/index.html)

Extract the zip to an appropriate location, such as /usr/local or /opt/

To launch, open termina go to the android-studio/bin directory, go to the location, and type
```bash
./studio.sh
```

Afterwards there should be an installation manager that pops up. Just follow the instructions there.