# OS: Windows 10 64-bit, Build 1607+

This is a guide / set of guides for installing software development tools in Windows 10. For this specific OS, we'll be discussing 2 possible ways to installing tools: By installing tools for Windows, or by installing tools for Linux through the Windows subsystem for Linux (or WSL).

Commands meant to be entered through the Bash terminal (WSL) look like this:
```bash
$ command_here
```

Commands meant to be entered through the Windows PowerShell or CMD look like this:
```bash
> command_here
```

Just message me (or anyone in charge of this) if there are discrepancies or errors.

 > Credits: Francis dela Cruz
