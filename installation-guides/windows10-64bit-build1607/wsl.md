# Windows Subsystem for Linux

This allows Windows 10 users to run Bash on Ubuntu on Windows. This is currently in beta as of writing (September 2017), so some features in Linux or Bash may not be available, and there might be bugs present.

A Virtual Machine or Dual-Boot system is still preferable, but this is a valid option, as when set up properly, you can use both Windows tools (eg. Text Editors, Image Processing) and Linux tools (eg. Compilers, Frameworks) in the same files.

## Instructions
> Taken from https://msdn.microsoft.com/en-us/commandline/wsl/install_guide

Open PowerShell as Administrator, then enter:
```bash
> Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
```

After running this, you'll be prompted to restart Windows. Restart, then continue afterwards.

Go to `Settings -> Update and Security -> For developers`, then click on `Select Developer mode`. This will install a package, so just wait a bit.

Open a command prompt (Win+R -> "cmd"), then you should be prompted to install Ubuntu on Windows. This will take a while, so just just walk through the dialog until you're done.

When you're prompted for a UNIX username and password, you can pick anything regardless of your Windows username and password.

Once you're done, you'll find yourself in your Windows home page in bash. Running `pwd` will output you current directory:
`/mnt/c/Users/<WindowsUsername>`

This means that you are accessing your Windows Directories (the `/c/Users/..`) through Ubuntu Bash (the `/mnt`). This will be the main way you'll be accessing files in Windows and Bash/Ubuntu.

## Post-installation Notes

The Linux distribution will be located at: `%localappdata%\lxss\`. **DO NOT EDIT, CREATE, OR EVEN OPEN FILES HERE USING WINDOWS TOOLS!** This can cause corruption and will most likely result in you having to reinstall WSL. Only make changes here through the Bash on WSL.

If you want to access Windows files in Bash, you can create the file in your own folder in Windows (eg. in `C:\Code\Projects`), then access it in Bash by going to `/mnt/c/Code/Projects`.

## Linux Crash Course

Most of the work you'll be doing when using Linux tools in Windows, so a crash course on UNIX commands and Bash usage will be helpful. You can check [this website](http://www.ee.surrey.ac.uk/Teaching/Unix/index.html) for a tutorial.

## Example workflow

In your folder `Documents\Work`, you write your C code in a file `code.c`. You can do this using text editors in Windows (eg. Notepad, Sublime Text, etc.). Then, in your Bash on WSL, you go to the directory `/mnt/c/Users/YourUsername/Documents/Work` and compile it using `gcc`, a tool in Linux for compiling C files. You can also run it in the Terminal afterwards:

```bash
$ gcc code.c
$ ./a.out
```

The files stay in the Windows directories, but the processing and compiling of the files are done in the Bash on WSL.

## Next steps

As of writing, the version of Ubuntu installed through the WSL is 16.04.2 LTS, so you should be able to install required tools using the commands in the Ubuntu 16.04 LTS installation guides here in the Dev-Resources. If you have any questions, feel free to raise a concern or contact the Engineering Committee.
