# Install script for Python, Django, Mezzanine, Node, SemanticUI.
# For Franco's DevCamp2016

#Install python, pip, Django, selenium, mezzanine and other python packages.
sudo apt-get update --yes
sudo apt-get upgrade --yes
sudo apt-get install --yes build-essential python python-pip python-dev python-imaging python-psycopg2
sudo apt-get install --yes python3.4 python3-pip python3-dev
sudo apt-get install --yes libffi-dev libssl-dev postgresql libpq-dev postgresql-client pgadmin3
sudo apt-get install --yes libjpeg8 libjpeg8-dev libjpeg62-turbo-dev
sudo apt-get build-dep python-imaging
sudo -H pip install --upgrade pip
sudo -H pip install --upgrade cffi
sudo -H pip install --upgrade setuptools
sudo -H pip install --upgrade pyopenssl ndg-httpsclient pyasn1
sudo -H pip install --upgrade Django
sudo -H pip install --upgrade selenium
sudo -H pip install --upgrade mezzanine
sudo -H pip install --upgrade virtualenv virtualenvwrapper
apt-get install curl
curl --silent --location https://deb.nodesource.com/setup_0.12 | sudo bash -
apt-get install --yes nodejs
