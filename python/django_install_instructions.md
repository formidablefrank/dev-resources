# DevCamp2016: Web dev using Django+SemanticUI+Mezzanine

## Install Django Instructions
Go to Linux terminal then follow the steps below:

Update software. Install python3.4, pip3, django1.8, mezzanine, among others.

```
sudo apt-get update --yes
sudo apt-get upgrade --yes
sudo apt-get install --yes build-essential python python-pip python-dev python-imaging python-psycopg2
sudo apt-get install --yes python3.4 python3-pip python3-dev
sudo apt-get install --yes libffi-dev libssl-dev postgresql libpq-dev postgresql-client pgadmin3
sudo apt-get install --yes libjpeg8 libjpeg8-dev libjpeg62-turbo-dev
sudo apt-get build-dep python-imaging
sudo -H pip install --upgrade pip
sudo -H pip install --upgrade cffi
sudo -H pip install --upgrade setuptools
sudo -H pip install --upgrade pyopenssl ndg-httpsclient pyasn1
sudo -H pip install --upgrade Django
sudo -H pip install --upgrade selenium
sudo -H pip install --upgrade mezzanine
sudo -H pip install --upgrade virtualenv virtualenvwrapper
```


Clone repo (using HTTPS or SSH):
```
git clone https://github.com/formidablefrank/**repo_name**.git
-or-
git clone git@github.com:formidablefrank/**repo_name**.git
```
***Repo URL varies***.
To clone using SSH, make sure you have submitted your public key in your account settings on GitHub.
SSH facilitates passwordless transactions.



Change working directory to repo
```
cd **repo_name**
```



Make virtualenv for the repo:
```
virtualenv env -p /usr/bin/python3.4
```



Activate virtualenv and each time you modify this project:
```
source env/bin/activate
```



Install project depedencies:
```
pip3 install -r requirements.txt
```



Django make migrations (after you update models and save objects):
```
python3.4 manage.py makemigrations
python3.4 manage.py migrate [--fake]
```



Django check for errors in codebase and run the server
```
python3.4 manage.py check
python3.4 manage.py runserver
```



In your web browser, go to http://localhost:8000/


# Development Reminders
Every time you edit Django projects, make sure you have pulled the repo first, update dependencies, and update your migrations.
```
git pull origin master (https)
git pull -u origin master (SSH)
pip3 install -r requirements.txt
python3 manage.py makemigrations
python3 manage.py migrate
```


##Testing
### Reminder: before testing the whole suite, please read functional_tests/tests.py first.
1. Testing the whole suite
2. Testing a specific case in the suite
3. Testing unit tests on the model

```
python3 manage.py test functional_tests
python3 manage.py test functional_tests.tests.Welcome.test_can_view_photos_in_current_scrapbook
python3 manage.py test scrapbook
```



# Other Stuff

Django-Python console:
```
python3.4 manage.py shell
```

You can install PyCharms Community Edition (IDE for Python/Django projects).
This is optional, you can use basic code editors like Atom and Sublime
https://www.jetbrains.com/pycharm/download/



Next best IDE is Atom text editor
https://atom.io



Usage of Django-extensions
https://github.com/django-extensions/django-extensions



Usage of Unipath
https://github.com/mikeorr/Unipath



Deploying Python webapps on Heroku with uWSGI
http://uwsgi-docs.readthedocs.org/en/latest/tutorials/heroku_python.html



Deployment with WhiteNoise
http://whitenoise.evans.io/en/latest/



# Customizing SemanticUI
Install NodeJS:
```
apt-get install curl
curl --silent --location https://deb.nodesource.com/setup_0.12 | sudo bash -
apt-get install --yes nodejs
```



Install Gulp and JQuery (http://semantic-ui.com/introduction/getting-started.html):
```
npm install -g gulp
npm update
npm install semantic-ui --save
```


Follow install instructions. When asked for a folder name, specify 'static\'
```
cd static/
gulp build
```


Gulp: watch for changes in style configs to update css/js files
```
gulp watch
gulp serve
gulp build
```

You can now modify 'static/src/theme.config'



# Installing Pillow Package
First, reinstall project dependencies as given above.

If you encounter
```
ValueError: --enable-jpeg requested but jpeg not found, aborting.
```
during installation of Pillow, do the following:


Install the following package, make a symlink then reinstall project dependencies again.
```
sudo apt-get install libjpeg62-turbo-dev
sudo ln -s /usr/lib/x86_64-linux-gnu/libjpeg.so /usr/lib
pip3 install -r requirements.txt
```


# Markdown Basics
Click the link: [Visit Github!](https://help.github.com/articles/markdown-basics/)
