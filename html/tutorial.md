# Learn Basic Git, HTML and CSS

UP CSI DevCamp 2019
October 23, 2019
Teaching Lab 2, 5:30PM - 7:00PM

## Prerequisites
- Working laptop, of course
- [Git](https://git-scm.com/downloads)
- [Visual Studio Code](https://code.visualstudio.com/download), for creating HTML documents
- [Google Chrome](https://www.google.com/chrome/), for debugging HTML documents
- [GitLab](https://gitlab.com/users/sign_in) account for code version control and collaboration
- For the purposes of this tutorial, a partner

## Git
1. Create a git repository and share it to your partner, with edit permissions
2. Clone the repository on both laptops

## Visual Studio
Install the following extensions (5/5 will definitely make our work easier):
- HTML Snippets
- Browser Preview
- GitLens
- Beautify