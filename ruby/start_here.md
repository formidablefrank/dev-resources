# Ruby (on Rails)

## Introduction

Ruby is an easy to use language (much like Python). Its recent popularity was due mostly to the framework Ruby on Rails, which allowed for fast web application development and a lower barrier of entry, with its abstractions and convention.

## Installation

Installation on Windows (even on WSL) is very cumbersome and oftentimes results to just installing a virtual machine running linux.

For Linux, there are multiple installation tutorials here in the Dev Resources for Ruby on Rails, the latest on ebeing [here.](https://gitlab.com/up-csi/dev-resources/blob/master/installation-guides/ubuntu-16.04-lts/rails_rbenv.md)

## Tutorials
* [This extensive book on Rails](https://www.railstutorial.org/book/) is a useful tutorial for newcomers to Rails.
* Notes for DevCamp 1718A on Ruby on Rails can be found [here.](https://drive.google.com/open?id=1e6bahvVEewH6QTNPfmA-8Iei3V3m_wStY7lHifgOVZ4)
