#For DCS TL2 use only! 32-bit Linux Mint with proxy
#Innov School 4.0: Ruby Rush
#J Franco Ray, Feb 25, '16

#Description: use this to install nodejs, rails, among others.

#Make sure you work at the $HOME directory.
cd

#Install some dependencies. Update and upgrade.
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install -y autoconf bison build-essential libssl-dev libyaml-dev
sudo apt-get install -y libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev
sudo apt-get install -y libgdbm3 libgdbm-dev
sudo apt-get install -y sqlite3 libsqlite3-dev
sudo apt-get install -y git nodejs curl

#Install nodejs, NVM.
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh | bash
chmod +x ~/.nvm/nvm.sh
~/.nvm/nvm.sh

# NOTE: Close all instances of the terminal and open it again before executing the following
nvm install stable
nvm use stable
nvm install iojs

#Install rbenv. Set ruby version 2.3.0.
git config --global core.autocrlf false
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
cd ~/.rbenv && src/configure && make -C src
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build

# NOTE: Close all instances of the terminal and open it again before executing the following
rbenv install 2.4.0
rbenv global 2.4.0

#Install rails and bundler
gem install rails
gem install bundler
