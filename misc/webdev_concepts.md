# Web Development Concepts and Tools

This section / page hopes to introduce web developers to some concepts behind buzzwords like "MVC" or "REST API", as well as tools like Git.

Do note that this section is meant to be an introduction to these concepts. Further research (depending on you and your project's needs) is advised.

## Git
* [The tutorial from the main Git website](https://git-scm.com/docs/gittutorial)
* [GitReady](http://gitready.com/) - A list of tips, but there doesn't seem to be a structured tutorial
* [The Git branching model we're trying to use](http://nvie.com/posts/a-successful-git-branching-model/)
* gitk - A nice visualization tool for Git logs and histories

## MVC (Model-View-Controller)
* [A nice short explanation to what MVC is](https://www.tomdalling.com/blog/software-design/model-view-controller-explained/)
* Do note that MVC is often discussed in the context of other frameworks

## REST API
* [Basic introduction for beginners](http://www.andrewhavens.com/posts/20/beginners-guide-to-creating-a-rest-api/)
* [REST API Tutorial](http://www.restapitutorial.com/) - A more in-depth tutorial

## JSON / JSON API
* [Wikipedia entry for JSON](https://en.wikipedia.org/wiki/JSON)
* [JSONPlaceholder](https://jsonplaceholder.typicode.com/) - a fake online JSON API useful for prototyping frontend pages
* [Postman](https://www.getpostman.com/) - a nice way to test JSON APIs and view the JSON responses
